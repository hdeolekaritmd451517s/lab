var express = require('express');
var router = express.Router();
var _ = require('underscore');

var products = [
	{
		id: 1,
		name: "Mac Book Pro",
		code:"00001",
		quantity: 5,
		color: "black",
		isAvailable: true
	},
	{
		id: 2,
		name: "Iphone",
		code:"00002",
		quantity: 10,
		color: "space grey",
		isAvailable: true
	},
	{
		id: 3,
		name: "Xbox",
		code:"00003",
		quantity: 0,
		color: "black",
		isAvailable: false
	},
	{
		id: 4,
		name: "Kinder Pad",
		code:"00004",
		quantity: 20,
		color: "black",
		isAvailable: true
	},
	{
		id: 5,
		name: "VR Gear Headset",
		code:"00005",
		quantity: 2,
		color: "black",
		isAvailable: true
	}
];


function lookupProduct(productId) {
  return _.find(products, function(p) {
    return p.id == parseInt(productId);
  });
}

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('products/index', { title: 'Product List', products: products });
});

router.get('/:productId', function(req, res, next) {
	var productId = req.params.productId;
		product = lookupProduct(productId);
		console.log(product);
		res.render('products/product', { title: 'Product Details', product: product});
});

module.exports = router;
